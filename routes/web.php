<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
\Illuminate\Support\Facades\Auth::routes();

Route::get('orders/category/{id}', 'OrderController@getSubCategory')->name('orders.category');
Route::get('orders/categories/', 'OrderController@categories')->name('orders.categories');
Route::get('category/products/{id}', 'OrderController@categoryProducts')->name('category.products');
Route::get('storeOrders/{id}', 'OrderController@storeOrder')->name('storeOrder');
Route::get('updateQuantity', 'OrderController@updateQuantity')->name('product.updateQuantity');
Route::get('deleteOrder', 'OrderController@deleteOrder')->name('deleteOrder');
Route::resource('orders', 'OrderController');

Route::get('/products', function () {

    return view('front.orders.products');
});

