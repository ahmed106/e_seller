<?php

use Illuminate\Support\Facades\Route;


Route::prefix('admin-panel')->middleware('auth:admin')->group(function () {

    Route::get('/', 'AdminController@index')->name('admin-panel');
    Route::get('/logout', 'Auth\AuthController@logout')->name('admin.logout');
    Route::get('categories/data', 'CategoryController@data')->name('categories.data');
    Route::get('categories/getParent', 'CategoryController@getParent')->name('categories.getParent');
    Route::get('categories/getFChildrenById', 'CategoryController@getFChildrenById')->name('categories.getFChildrenById');
    Route::get('categories/getTChildrenById', 'CategoryController@getTChildrenById')->name('categories.getTChildrenById');
    Route::get('categories/getRChildrenById', 'CategoryController@getRChildrenById')->name('categories.getRChildrenById');
    Route::get('categories/getChildren', 'CategoryController@getChildren')->name('categories.getChildren');
    Route::resource('categories', 'CategoryController');
    Route::get('products/data', 'ProductController@data')->name('products.data');
    Route::resource('products', 'ProductController');
});
Route::prefix('admin-panel')->middleware('guest:admin')->group(function () {

    Route::get('/login', 'Auth\AuthController@login')->name('admin.login');
    Route::post('/login', 'Auth\AuthController@doLogin')->name('admin.doLogin');
});

