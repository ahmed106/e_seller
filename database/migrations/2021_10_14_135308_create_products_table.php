<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('photo')->nullable();
            $table->unsignedInteger('f_category_id')->nullable();
            $table->unsignedInteger('s_category_id')->nullable();
            $table->unsignedInteger('t_category_id')->nullable();
            $table->integer('r_category_id')->nullable();
            $table->text('description');
            $table->integer('price');
            $table->integer('quantity')->default(0);
            $table->foreign('f_category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('s_category_id')->references('id')->on('s_parent_id')->onDelete('cascade');
            $table->foreign('t_category_id')->references('id')->on('s_parent_id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
