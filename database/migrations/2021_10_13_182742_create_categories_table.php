<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->unsignedInteger('f_parent_id')->nullable();
            $table->unsignedInteger('s_parent_id')->nullable();
            $table->unsignedInteger('t_parent_id')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
            $table->foreign('f_parent_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('s_parent_id')->references('f_parent_id')->on('categories')->onDelete('set null');
            $table->foreign('t_parent_id')->references('s_parent_id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
