<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return $this->photo != null ? asset('images/categories/' . $this->photo) : asset('default.png');

    }//end of  function


    public function products()
    {
        return $this->hasMany(Product::class);


    }//end of products function


}
