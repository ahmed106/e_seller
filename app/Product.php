<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return $this->photo != null ? asset('images/products/' . $this->photo) : asset('default.png');

    }//end of  function

    public function firstCategory()
    {
        return $this->belongsTo(Category::class, 'f_category_id');


    }//end of category function

    public function secondCategory()
    {
        return $this->belongsTo(Category::class, 's_category_id');


    }//end of category function

    public function thirdCategory()
    {
        return $this->belongsTo(Category::class, 't_category_id');


    }//end of category function

    public function getParent($id)
    {
        return Category::where('id', $id)->first()->name;


    }//end of category function

}
