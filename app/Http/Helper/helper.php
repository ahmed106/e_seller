<?php

if (!function_exists('active')) {

    function active($url)
    {

        if (request()->segment(1) == $url) {

            return 'active';
        } elseif (request()->segment(2) == $url) {
            return 'active';
        } else {
            return '';
        }
    }
}
