<?php

namespace App\Http\Controllers\Admins\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('admins.auth.login');

    }//end of login function

    public function doLogin(Request $request)
    {

        $credentials = $request->only(['email', 'password']);

        if (auth('admin')->attempt($credentials)) {

            return redirect()->intended(route('admin-panel'));
        } else {
            return redirect()->back();
        }
    }//end of doLogin function

    public function logout()
    {

        auth('admin')->logout();

        return redirect('/admin-panel/login');

    }//end of logout function
}
