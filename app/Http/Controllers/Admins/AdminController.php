<?php

namespace App\Http\Controllers\Admins;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;

class AdminController extends Controller
{
    public function index()
    {

        $products = Product::all();
        $categories = Category::all();
        return view('admins.index', compact('products', 'categories'));


    }//end of index function
}
