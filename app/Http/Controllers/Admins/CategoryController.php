<?php

namespace App\Http\Controllers\Admins;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function index()
    {

        return view('admins.categories.index');
    }//end of index function

    public function data()
    {
        $categories = Category::get();

        return DataTables::of($categories)
            ->addColumn('actions', function ($row) {
                return view('admins.categories.actions', compact('row'));
            })
            ->make(true);

    }//end of data function

    public function create()
    {
        return view('admins.categories.create');


    }//end of create function

    public function store(Request $request)
    {


        $data = $request->except(['_token']);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

//        if ($request->s_parent_id != null) {
//            $data['f_parent_id'] = null;
//        }
//        if ($request->t_parent_id != null) {
//            $data['f_parent_id'] = null;
//            $data['s_parent_id'] = null;
//        }
        if ($request->hasFile('photo')) {
            $name = time() . '_' . $request->photo->getClientOriginalName();
            $request->photo->move(public_path('images/categories'), $name);
            $data['photo'] = $name;
        }
        Category::create($data);

        return redirect()->route('categories.index');


    }//end of store function

    public function edit($id)
    {
        $cat = Category::findOrFail($id);
        return view('admins.categories.edit', compact('cat'));


    }//end of edit function

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $cat = Category::find($id);

        if ($request->hasFile('photo')) {
            File::delete(public_path('images/categories/' . $cat->photo));
            $name = time() . '_' . $request->photo->getClientOriginalName();
            $request->photo->move(public_path('images/categories'), $name);
            $data['photo'] = $name;
        }
        $cat->update($data);
        return redirect()->route('categories.index');


    }//end of update function

    public function destroy($id)
    {

        $cat = Category::find($id);
        $cat->delete();
        return redirect()->route('categories.index');


    }//end of destroy function

    public function getParent()
    {
        $categories = Category::where('f_parent_id', null)->get();
        return response()->json(['data' => $categories], 200);

    }//end of getParent function

    public function getChildren()
    {

        $categories = Category::where('s_parent_id', '!=', null)->get();
        return response()->json(['data' => $categories], 200);
    }//end of getChildren function

    public function getFChildrenById(Request $request)
    {

        $id = $request->cat_id;
        $categories = Category::where('f_parent_id', $id)->where('s_parent_id', null)->get();
        return response()->json(['data' => $categories], 200);


    }//end of getChildrenById function

    public function getTChildrenById(Request $request)
    {
        $id = $request->cat_id;
        $categories = Category::where('s_parent_id', $id)->where('t_parent_id', null)->get();
        return response()->json(['data' => $categories], 200);

    }//end of getThirdCat function

    public function getRChildrenById(Request $request)
    {
        $id = $request->cat_id;
        $categories = Category::where('t_parent_id', $id)->get();
        return response()->json(['data' => $categories], 200);

    }//end of getThirdCat function
}
