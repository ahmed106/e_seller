<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    public function index()
    {
        return view('admins.products.index');


    }//end of index function

    public function data()
    {

        $products = Product::all();

        return DataTables::of($products)
            ->addColumn('actions', function ($row) {
                return view('admins.products.actions', compact('row'));
            })
            ->make(true);

    }//end of data function

    public function create()
    {
        return view('admins.products.create');


    }//end of create function

    public function store(Request $request)
    {


        $request->validate([
            'f_category_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'photo' => 'image|mimes:png,jpg,jpeg'
        ]);
        $data = $request->except(['_token', 'photo']);
        if ($request->hasFile('photo')) {
            $name = time() . '_' . $request->photo->getClientOriginalName();
            $request->photo->move(public_path('images/products'), $name);
            $data['photo'] = $name;
        }

        $prod = Product::create($data);

        return redirect()->route('products.index');
    }//end of store function

    public function edit($id)
    {
        $product = Product::findOrfail($id);

        return view('admins.products.edit', compact('product'));


    }//end of edit function

    public function update(Request $request, $id)
    {

        $data = $request->except(['_token', 'photo']);
        $request->validate([

            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'f_category_id' => 'required',
            's_category_id' => 'required',
            'photo' => 'image|mimes:png,jpg,jpeg',
        ]);
        $product = Product::findOrFail($id);
        if ($request->hasFile('photo')) {
            File::delete(public_path('images/products/' . $product->photo));
            $name = time() . '_' . $request->photo->getClientOriginalName();
            $request->photo->move(public_path('images/products'), $name);
            $data['photo'] = $name;
        }
        $product->update($data);
        return redirect()->route('products.index');

    }//end of update function

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        File::delete(public_path('images/products/' . $product->photo));
        $product->delete();
        return redirect()->route('products.index');

    }//end of destroy function


}
