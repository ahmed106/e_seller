<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{

    public function index()
    {

        return view('front.orders.categories');
    }//end of index function

    public function store(Request $request)
    {

        $data = $request->except('_token');
        $order = Order::create($data);
        Session::put('order', $order);
        return redirect()->route('orders.categories');

    }//end of store function

    public function categories()
    {

        $categories = Category::where('f_parent_id', null)->get();

        return view('front.orders.categories', compact('categories'));

    }//end of categories function

    public function getSubCategory($id)
    {

        $category = Category::findOrFail($id);
        $subCategories = Category::where('f_parent_id', $category->id)->where('s_parent_id', null)->get();
        return view('front.orders.subcategories', compact('subCategories'));

    }//end of  function

    public function categoryProducts($id)
    {
        $cat_id = Category::findOrFail($id);
        $sub = Category::where('s_parent_id', $cat_id->id)->where('t_parent_id', null)->get();
        $third = Category::where('t_parent_id', $cat_id->id)->get();


        if ($sub->count() > 0) {

            $subCategories = Category::where('s_parent_id', $cat_id->id)->where('t_parent_id', null)->get();
            return view('front.orders.subcategories', compact('subCategories'));
        }
        if ($third->count() > 0) {
            $subCategories = Category::where('t_parent_id', $cat_id->id)->get();

            return view('front.orders.subcategories', compact('subCategories'));
        }


        $products = Product::where('r_category_id', $cat_id->id)->orWhere('s_category_id', $cat_id->id)->where('quantity', '>', 0)->get();


        return view('front.orders.products', compact('products'));

    }//end of categoryProducts function

    public function storeOrder($id)
    {
        $product = Product::findOrFail($id);
        $order = Session::get('order');
        $order->update([
            'product_name' => $product->name,
            'price' => $product->price,
            'quantity' => 1,
        ]);
        return view('front.orders.print', compact('order'));


    }//end of storeOrder  function

    public function updateQuantity(Request $request)
    {
        $order = Session::get('order');
        $order->update([
            'quantity' => $request->quantity,
        ]);

    }//end of updateQuantity function

    public function deleteOrder()
    {

        $order = Session::get('order');
        $order->delete();
        Session::forget('order');
        return response()->json('success', 'order deleted');

    }//end of deleteOrder function
}
