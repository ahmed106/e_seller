<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Directing Template">
    <meta name="keywords" content="Directing, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Directing | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/style.css" type="text/css">
</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<header class="header">


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="./index.html"><img src="{{asset('assets/front')}}/img//logo.png" alt=""></a>
                </div>
            </div>


            <!-- Most Search Section Begin -->
            <div class="col-lg-12 col-md-12 col-12">
                <a href="{{url('/')}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-alt-circle-left"></i> الرجوع للقائمه </a>
                <a id="cancle_btn" href="{{url('/')}}" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> الغاء العمليه </a>
                <h2 class="text-center mb-5">طباعه الفاتوره</h2>


                <div id="print_table">

                    <label for="">اسم العميل</label>
                    <input type="text" value="{{$order->name??''}}" readonly>
                    <label for="">الهاتف</label>
                    <input type="text" value="{{$order->phone??''}}" readonly>
                    <table class="table table-hover table-striped text-center" id="">
                        <thead>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>price</th>
                        <th>quantity</th>

                        </thead>
                        <tbody>

                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->product_name}}</td>
                            <td id="price" data-price="{{$order->price}}">{{$order->price}}</td>
                            <td><input type="number" min="{{$order->quantity}}" value="{{$order->quantity}}" id="quantity"></td>


                        </tr>
                        </tbody>

                    </table>

                    <div class="text-center">

                        <p>test </p>
                    </div>

                </div>


                <a href="" class="btn btn-success btn-sm form-control" id="print"><i class="fa fa-print"></i> Print</a>


            </div>
            <!-- Most Search Section End -->
        </div>
    </div>
</header>

<!-- Js Plugins -->
<script src="{{asset('assets/front')}}/js/jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/front')}}/js/bootstrap.min.js"></script>
<script src="{{asset('assets/front')}}/js/jquery.nice-select.min.js"></script>
<script src="{{asset('assets/front')}}/js/jquery-ui.min.js"></script>
<script src="{{asset('assets/front')}}/js/jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/front')}}/js/jquery.barfiller.js"></script>
<script src="{{asset('assets/front')}}/js/jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/front')}}/js/jquery.slicknav.js"></script>
<script src="{{asset('assets/front')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('assets/front')}}/js/main.js"></script>
<script src="{{asset('assets/front')}}/js/printThis.js"></script>
<script src="https://kit.fontawesome.com/2eb6657f57.js" crossorigin="anonymous"></script>

<script>
    $(function () {
        $('#print').on('click', function (e) {
            e.preventDefault();
            $('#print_table').printThis()

        });
        $('#quantity').on('change', function () {
            $('#price').empty();
            $('#price').append($('#price').data('price') * $(this).val())

            $.ajax({
                type: 'get',
                url: '{{route("product.updateQuantity")}}',
                data: {
                    'quantity': $(this).val(),
                }
            })
        });

        $('#cancle_btn').on('click', function (e) {
            e.preventDefault();

            $.ajax({

                type: 'get',
                url: '{{route('deleteOrder')}}',

                success: function (response) {
                    window.location.href = '/'
                }
            })
            window.location.href = '/'
        })
    })
</script>
</body>
