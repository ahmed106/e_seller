<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Directing Template">
    <meta name="keywords" content="Directing, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Directing | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/style.css" type="text/css">
</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{url('/')}}"><img src="{{asset('assets/front')}}/img//logo.png" alt=""></a>
                </div>
            </div>

        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>


<!-- Hero Section Begin -->
<section class="hero set-bg" data-setbg="{{asset('tire.jpg')}}" style="height: 913px">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">

                </div>
                <div class="categories__item__list">
                    @foreach($categories as $category)
                        <a href="{{route('orders.category',$category->id)}}">
                            <div class="categories__item">
                                <img src="{{$category->image_path}}" style=" width: 200px; height: 200px" alt="">
                                <h5>{{$category->name}}</h5>

                            </div>
                        </a>
                    @endforeach {{-- end of categories foreach --}}


                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->

<!-- Categories Section End -->

<!-- Js Plugins -->
<script src="{{asset('assets/front')}}/js//jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/front')}}/js//bootstrap.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nice-select.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery-ui.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.barfiller.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.slicknav.js"></script>
<script src="{{asset('assets/front')}}/js//owl.carousel.min.js"></script>
<script src="{{asset('assets/front')}}/js//main.js"></script>
</body>

</html>
