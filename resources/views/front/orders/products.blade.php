<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Directing Template">
    <meta name="keywords" content="Directing, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Directing | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/style.css" type="text/css">
</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<header class="header">


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="./index.html"><img src="{{asset('assets/front')}}/img//logo.png" alt=""></a>
                </div>
            </div>


            <!-- Most Search Section Begin -->
            <section class="most-search spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h2>The Most Searched Services</h2>


                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">


                            <div class="row">
                                @foreach($products as  $product)
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{route('storeOrder',$product->id)}}">
                                            <div class="listing__item">

                                                <div class="listing__item__pic set-bg" data-setbg="{{$product->image_path}}" style="height: 300px">
                                                    <img src="{{asset('assets/front')}}/img/listing/list_icon-1.png" alt="">

                                                    <div class="listing__item__pic__btns">

                                                    </div>
                                                </div>
                                                <div class="listing__item__text">
                                                    <div class="listing__item__text__inside">
                                                        <h5>{{$product->name}}</h5>
                                                        <div class="listing__item__text__rating">

                                                            <p>{{$product->description}}</p>
                                                        </div>

                                                    </div>
                                                    <div class="listing__item__text__info">
                                                        <div class="listing__item__text__info__left">

                                                            <img src="{{asset('assets/front')}}/img/listing/list_small_icon-1.png" alt="">
                                                            <span>{{$product->firstCategory->name??''}} </span>
                                                            <p> {{$product->secondCategory->name??''}}{{$product->thirdCategory?  ' / '.$product->thirdCategory->name :''}}
                                                                {{$product->r_category_id? ' / '.$product->getParent($product->r_category_id) :''}}</p>
                                                        </div>
                                                        <div class="listing__item__text__info__right">${{$product->price}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                @endforeach {{-- end of category->products foreach --}}


                            </div>


                        </div>
                    </div>
                </div>
            </section>
            <!-- Most Search Section End -->
        </div>
    </div>
</header>

<!-- Js Plugins -->
<script src="{{asset('assets/front')}}/js//jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/front')}}/js//bootstrap.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nice-select.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery-ui.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.barfiller.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.slicknav.js"></script>
<script src="{{asset('assets/front')}}/js//owl.carousel.min.js"></script>
<script src="{{asset('assets/front')}}/js//main.js"></script>
</body>
