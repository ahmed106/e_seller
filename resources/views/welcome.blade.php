<!DOCTYPE html>
<html lang="zxx">

<head>


    <meta charset="UTF-8">
    <meta name="description" content="Directing Template">
    <meta name="keywords" content="Directing, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Directing | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/css/style.css" type="text/css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@700&display=swap" rel="stylesheet">

    <style>

        input, button, label, h1, h2, h3, h4, h5 {
            font-family: 'Cairo', sans-serif;
        }

        input {
            text-align: center;
        }

    </style>


</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{url('/')}}"><img src="{{asset('assets/front')}}/img//logo.png" alt=""></a>
                </div>
            </div>

        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>

<!-- Hero Section Begin -->
<section class="hero set-bg" data-setbg="{{asset('tire.jpg')}}" style="height: 980px">
    <div class="container">
        <div class="row">
            <div class="col-lg-6" style="margin-top:28%;left: 25%">
                <div class="hero__text">
                    <form action="{{route('orders.store')}}" method="post">
                        @csrf
                        <div class="form-group hero__search__form">
                            <div class="section-title">
                                <h4 style="color: #f03250">من فضلك أدخل</h4>
                            </div>
                            <input name="name" type="text" class="form-control" style="width: 100%" placeholder="الاسم">
                            <input name="phone" type="number" class="form-control mt-4" style="width: 100%" placeholder="رقم الهاتف">


                            <div class="section-title mt-3">

                                <button class="btn btn-primary" type="submit">التالي</button>
                                <button class="btn btn-danger" type="submit">تخطي</button>
                            </div>


                        </div>


                    </form>


                </div>
            </div>

        </div>
    </div>
</section>
<!-- Hero Section End -->


<!-- Js Plugins -->
<script src="{{asset('assets/front')}}/js//jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/front')}}/js//bootstrap.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nice-select.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery-ui.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.barfiller.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/front')}}/js//jquery.slicknav.js"></script>
<script src="{{asset('assets/front')}}/js//owl.carousel.min.js"></script>
<script src="{{asset('assets/front')}}/js//main.js"></script>
</body>

</html>
