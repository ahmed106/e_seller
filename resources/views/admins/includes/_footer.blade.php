<footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}} <a href="">Ahmed Issa</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <a href="{{route('admin.logout')}}" class="btn btn-danger btn-sm"><i class="fa fa-power-off"></i></a>
    </div>
</footer>
