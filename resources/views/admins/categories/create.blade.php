@extends('admins.layouts.master')
@push('styles')

    <link rel="stylesheet" href="{{asset('assets/admin')}}/plugins/summernote/summernote-bs4.css">
@endpush
@section('content_header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">الاقسام</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item "><a href="">الرئيسيه</a></li>

                        <li class="breadcrumb-item"><a href="">الاقسام</a></li>
                        <li class="breadcrumb-item active">قسم جديد</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <form id="category_form" class="form-horizontal" action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mt-5">
                    <div class="col-md-4">
                        <div class="card card-primary card-outline pb-5">
                            <div class="form-group container">
                                <label for="">القسم</label>
                                <select name="f_parent_id" id="parent" class="" data-placeholder="القسم" style="width: 90%;">

                                    <option value="">قسم رأيسي</option>

                                </select>
                                @error('f_parent_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                        </div>

                        <div class="card card-primary card-outline pb-5 d-none" id="sub_cat">
                            <div class="form-group container">
                                <label for="">القسم</label>
                                <select name="s_parent_id" id="subcategory" class="" data-placeholder="القسم" style="width: 90%;">


                                </select>
                                @error('s_parent_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                        </div>

                        <div class="card card-primary card-outline pb-5 d-none " id="thirdCate">
                            <div class="form-group container">
                                <label for="">القسم</label>
                                <select name="t_parent_id" id="thirdsubcategory" class="" data-placeholder="القسم" style="width: 90%;">

                                    <option value="">قسم فرعي</option>

                                </select>
                            </div>

                        </div>
                        <div class="card card-primary card-outline ">

                            <div class="card-body box-profile">
                                Photo
                                <div class="text-center">
                                    <img id="image_preview" class="profile-user-img img-fluid img-circle" style="width: 150px;height: 150px" src="{{asset('default.png')}}" alt="User profile picture">
                                </div>


                                <div class="row">
                                    <input id="image" class="col-md-9" type="file" name="photo">

                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-md-8">

                        <div class="card card-primary card-outline pb-5">
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">

                                    <div class="tab-pane fade active show" id="custom-tabs" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">


                                        <div class="form-group">
                                            <label for="">الاسم</label>

                                            <input name="name" class="form-control" id="">
                                            @error('name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">الوصف</label>

                                            <textarea name="description" cols="125" rows="5" id=""></textarea>
                                            @error('description')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.row -->

                <button class="btn btn-success form-control" type="submit">حفظ</button>
            </form>
        </div>


    </section>
@endsection

@push('scripts')
    <script src="{{asset('assets/admin')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <script>

        $.ajax({

            type: 'get',
            url: '{{route('categories.getParent')}}',

            success: function (response) {

                $(response.data).each(function (key, value) {
                    $('#parent').append(`<option value="${value.id}">${value.name}</option>`)
                })

            }
        })


    </script>
    <script>

        $('#image').on('change', function (e) {

            let file = e.target.files[0],
                url = URL.createObjectURL(file);

            $('#image_preview').attr('src', url)

        })
    </script>
    <script>

        $('#parent').on('change', function () {
            $('#subcategory').empty();
            let cat_id = $(this).val();


            $.ajax({
                type: "get",
                url: '{{route('categories.getFChildrenById')}}',
                data: {
                    'cat_id': cat_id,
                },
                success: function (response) {

                    if (response.data.length > 0) {
                        let html = `<option value="">----</option>`
                        $.each(response.data, function (key, value) {
                            html += `<option value="${value.id}">${value.name}</option>`
                            $('#sub_cat').removeClass('d-none')
                            $('#subcategory').html(html)


                        })
                    } else {
                        $('#sub_cat').addClass('d-none')

                    }
                }
            })
        })
    </script>

    <script>
        $('#subcategory').on('change', function () {
            $('#thirdsubcategory').empty();
            let cat_id = $(this).val();


            $.ajax({
                type: "get",
                url: '{{route('categories.getTChildrenById')}}',
                data: {
                    'cat_id': cat_id,
                },
                success: function (response) {

                    if (response.data.length > 0) {
                        let html = `<option value="">----</option>`
                        $.each(response.data, function (key, value) {
                            html += `<option value="${value.id}">${value.name}</option>`
                            $('#thirdCate').removeClass('d-none')
                            $('#thirdsubcategory').append(html)


                        })
                    } else {
                        $('#thirdCate').addClass('d-none')

                    }
                }
            })
        })
    </script>
@endpush
