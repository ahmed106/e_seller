<a class=" btn btn-sm btn-primary d-inline-block" href="{{route('categories.edit',$row->id)}}" title="Edit"><i class="fa fa-edit"></i></a>

<form class="d-inline-block" onclick="return(confirm('are you sure !'))" action="{{route('categories.destroy',$row->id)}}" method="post">
    @csrf
    @method('delete')

    <button class=" d-inline-block btn-sm btn btn-danger   " type="submit" title="Delete"><i class="fa fa-trash"></i></button>

</form>
