@extends('admins.layouts.master')
@push('styles')
    <!-- DataTables -->

    <link rel="stylesheet" href="{{asset('assets/admin')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">


@endpush


@section('content_header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">المنتجات</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">الرئسيه</li>
                        <li class="breadcrumb-item"><a href="#">المنتجات</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('content')
    <section class="content">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title pull-right"><a class="btn btn-primary" href="{{route('products.create')}}"><i class="fa fa-plus"></i>منتج جديد </a></h3>

            </div>

            <!-- /.card-header -->
            <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table text-center table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>اسم القسم</th>
                                    <th>السعر</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>

    </section>
@endsection
@push('scripts')

    <script src="{{asset('assets/admin')}}/plugins/datatables/jquery.dataTables.js"></script>
    <script src="{{asset('assets/admin')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

    <script>

        $(function () {

            $('#example1').DataTable({
                "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
                "processing": true,
                "serverSide": true,
                //  'responsive': true,
                "ajax": {
                    'type': 'get',
                    'url': '{{route('products.data')}}'
                },
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'price', name: 'price'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]

            });
        });


    </script>
@endpush
